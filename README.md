# Lokale Gitlab-Instanz mit gitlab-runner

Einrichtung mit docker-compose, angelehnt an die Anleitung in
<https://www.philipp-doblhofer.at/blog/gitlab-server-mit-docker-und-ci-cd/>

## Installation von docker und docker-compose

_in diesem Beispiel: unter Debian oder Ubuntu_

```
sudo apt install docker.io docker-compose
```

## Vorbereiten der compose-datei

[docker-compose.yml](./docker-compose.yml)

Eintrag des Hostnamens, der via Eintrag in `/etc/hosts` lokal erreichbar gemacht wird,
unter services/gitlab/hostname und services/gitlab/environment/GITLAB\_OMNIBUS\_CONFIG
in die YAML-Datei, in diesem Fall `gitlab.esplendor.local`

Erzeugung der in die Container gemounteten Verzeichnisse

```
sudo mkdir -p /srv/composed/{gitlab/{config,logs,data},gitlab-runner}
```

## Starten der Container

```
docker-compose up -d
```

Das Setup von Gitlab dauert eine ganze Weile.

Anzeige der Logs (fortlaufend):

```
docker-compose logs -f
```

## Registrieren des Gitlab runner

```
docker exec -it gitlab-runner gitlab-runner register
```

und Angabe der Gitlab-URL (hier: `http://gitlab.esplendor.local/`),
des _registration token_ aus der GitLab-GUI, der Beschreibung und der Tags.

## Restart

Da in der Konfiguration des Runners im Gegensatz zu GitLab selbst
**nicht** `restart: always` angegeben wurde
(vermutlich, um fehlerhafte Verbindungversuche vor dem vollständigen Start des
GitLab-Containers zu unterbinden), muss nach einem Restart der Runner über

```
docker-compose up -d
```

nachgezogen werden.

## Troubleshooting

### Gitlab Runner: could not resolve host (beim git pull)

siehe <https://gitlab.com/gitlab-org/gitlab-runner/-/issues/6644#note_356811900>

```
$ docker inspect --format='{{.NetworkSettings.Networks}}' gitlab-runner
map[local-gitlab-with-runner_internal_network:0xc00052c000]
```

Das Netzwerk (in diesem Fall `local-gitlab-with-runner_internal_network`)
in die config.toml des Runner eintragen:

```
$ sudo cat /srv/composed/gitlab-runner/config.toml 
(…)
  [runners.docker]
    network_mode = "local-gitlab-with-runner_internal_network"
(…)
```


